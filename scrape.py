#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
import re
import os


def scrape_for_lyrics(
    url="https://genius.com/Dragonforce-through-the-fire-and-flames-lyrics",
):
    page = requests.get(
        url,
        headers={"User-Agent": "Requests"},
    )

    html = BeautifulSoup(page.text, "lxml")
    lyrics = html.find("div", class_="Lyrics__Container-sc-1ynbvzw-6 YYrds").get_text(
        separator="\n"
    )

    lyrics = re.sub(r"[\(\[].*?[\)\]]", "", lyrics)
    lyrics = os.linesep.join([s for s in lyrics.splitlines() if s])

    return lyrics
