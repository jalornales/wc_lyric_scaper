import requests
import json

"""
get the artists' ID
"""


def get_artists_id():
    pass


"""
Will get the data from json using rapid api
"""


def get_and_write_artists_data_to_file(id="162090"):
    # RapidApi for accessing genius.coms api
    # base_url = "https://genius.p.rapidapi.com/artists/162090/songs"
    url = "https://genius.p.rapidapi.com/artists/" + id + "/songs"

    querystring = {"per_page": "20", "sort": "popularity"}

    headers = {
        "X-RapidAPI-Key": "<YOUR_KEY_HERE>",
        "X-RapidAPI-Host": "<YOUR_HOST_HERE>",
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    dictionary = response.json()

    print("writing to data/" + id + ".json")
    with open("data/" + id + ".json", "w") as outfile:
        json.dump(dictionary, outfile)


"""
parse the urls of the json
"""


def get_urls_from_artists_json(id="162090"):
    FILE = open("data/" + id + ".json")
    data = json.load(FILE)
    songs = data["response"]["songs"]

    urls = []
    for item in songs:
        if item["lyrics_state"] == "complete":
            urls.append(item["url"])

    return urls
