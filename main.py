#!/usr/bin/env python3

import os
import itertools
import re
from collections import Counter

import genius
import scrape

"""
This will process the text file to unigram
"""


def unigram(id="162090"):
    FILE = open("data/" + id + ".txt", "r")

    file_lines = FILE.readlines()

    # definite and indefinite article
    black_list = ["the", "a", "an", "and", "of",]

    processed_file = []
    for line in file_lines:
        line = re.sub(r"[^a-zA-Z0-9 ]", "", line)
        processed_file.append(line.split(" "))

    # join lists of list to a single list
    processed_file = list(itertools.chain.from_iterable(processed_file))

    processed_text = []
    for word in processed_file:
        processed_text.append(word.lower())

    # remove blank string
    while "" in processed_text:
        processed_text.remove("")

    # remove articles 'the', 'a', 'an'
    for word in processed_text:
        for filter in black_list:
            if filter == word:
                processed_text.remove(word)

    word_counts = Counter(processed_text)

    print(word_counts.most_common(100))


def write_lyrics(id="162090"):
    print("checking for id: " + id)
    print("looking at data/" + id + ".json")
    file_exists = os.path.exists("data/" + id + ".json")
    if not file_exists:
        print("getting artist's id...")
        # save a data of artist in json file on data dir
        genius.get_and_write_artists_data_to_file(id)

    print("found data/" + id + ".json")

    print("getting all urls")
    urls = genius.get_urls_from_artists_json(id)

    combined_lyrics = []
    for url in urls:
        print("checking for: " + url)
        combined_lyrics.append(scrape.scrape_for_lyrics(url))

    with open("data/" + id + ".txt", "w") as file:
        for line in combined_lyrics:
            file.write(line)


def main() -> None:
    # get artist's id
    # id = genius.get_artists_id("Dragonforce")
    id = "162090"  # Dragonforce's ID

    if not os.path.exists("data/" + id + ".txt"):
        print(id + " not exists, will fetch lyrics now...")
        write_lyrics(id)
    else:
        print(id + " exists, checking unigram")

    unigram(id)


if __name__ == "__main__":
    main()
